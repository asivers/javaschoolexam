package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here

        // checking if string is null
        if (statement == null)
            return null;
        // checking for wrong characters
        if (generalPattern(statement)) {
            // add brackets to calculate using brackets method
            statement = "(" + statement + ")";
            double res = 0;
            String result = "";
            try {
                // rounding to 4 digits
                res = Math.round((Double.parseDouble(brackets(statement))) * 10000.0) / 10000.0;
                if ((res == Math.floor(res)) && !Double.isInfinite(res))
                    // the whole answer will be displayed without a fraction
                    result = Integer.toString((int)res);
                else
                    result = Double.toString(res);
            }
            catch (Exception e) {
                return null;
            }
            return result;
        }
        else
            return null;
    }

    // checking for wrong characters
    public boolean generalPattern(String str) {
        return str.matches("[[0-9]-*/\\.\\+\\(\\)]+");
    }

    public String brackets(String str) {
        String x = "";
        String inB = "";
        int xLen = 0;
        int xStart = 0;
        while (true) {
            // look for the innermost brackets (without other brackets inside)
            Matcher m  = Pattern.compile("\\([^\\(\\)]+\\)").matcher(str);
            if (m.find()) {
                x = m.group();
                xLen = x.length();
                xStart = m.start();
                x = x.substring(1, xLen - 1);
                // calculate the result inside the brackets and replace the brackets with the result of the calculation
                inB = inBrackets(x);
                str = str.substring(0, xStart) + inB + str.substring(xLen + xStart);
                // separately handle the case when one negative number remains inside the brackets
                if (inB.charAt(0) == '-' && xStart > 0)
                    str = str.substring(0, xStart) + "n" + str.substring(xStart);
            }
            else
                break;
        }
        return str;
    }

    // calculation inside brackets
    public String inBrackets(String str) {
        // list for numbers
        List<String> numbers = new ArrayList<>();
        // list for math operations
        List<String> operations = new ArrayList<>();
        // in case the expression starts with a minus
        numbers.add("0");
        if (str.charAt(0) == '-') {
            operations.add("-");
            str = str.substring(1);
        }
        else {
            operations.add("+");
        }
        String x = "";
        while (true) {
            // looking for numbers
            Matcher m  = Pattern.compile("([0-9]+\\.?[0-9]+)|[0-9]").matcher(str);
            if (m.find() && m.start() == 0) {
                x = m.group();
                numbers.add(x);
                str = str.substring(x.length());
            }
            // handle the case with the only negative number in the bracket, save it in an array of numbers with a minus sign
            else if (str.charAt(0) == 'n') {
                str = str.substring(2);
                m  = Pattern.compile("([0-9]+\\.?[0-9]+)|[0-9]").matcher(str);
                if (m.find() && m.start() == 0) {
                    x = m.group();
                    numbers.add("-" + x);
                    str = str.substring(x.length());
                }
                else
                    throw new ArithmeticException();
            }
            else
                throw new ArithmeticException();
            // looking for math operations
            m  = Pattern.compile("[-*/\\+]").matcher(str);
            if (m.find() && m.start() == 0) {
                x = m.group();
                operations.add(x);
                str = str.substring(x.length());
            }
            else
            if (str.length() == 0)
                break;
            else
                throw new ArithmeticException();
        }
        double res = 0;
        // carry out calculations, observing the order of actions. Lists shrink as you go
        while (operations.size() > 0) {
            for (int i = 0; i < operations.size(); i++) {
                if ((operations.get(i).equals("*")) || (operations.get(i).equals("/"))) {
                    if (operations.get(i).equals("*")) {
                        res = Double.parseDouble(numbers.get(i)) * Double.parseDouble(numbers.get(i + 1));
                        numbers.set(i + 1, Double.toString(res));
                        numbers.remove(i);
                        operations.remove(i);
                        break;
                    }
                    if (operations.get(i).equals("/")) {
                        if (Double.parseDouble(numbers.get(i + 1)) == 0)
                            throw new ArithmeticException();
                        else
                            res = Double.parseDouble(numbers.get(i)) / Double.parseDouble(numbers.get(i + 1));
                        numbers.set(i + 1, Double.toString(res));
                        numbers.remove(i);
                        operations.remove(i);
                        break;
                    }
                }
            }
            for (int i = 0; i < operations.size(); i++) {
                if ((operations.get(i).equals("+")) || (operations.get(i).equals("-"))) {
                    if (operations.get(i).equals("+")) {
                        res = Double.parseDouble(numbers.get(i)) + Double.parseDouble(numbers.get(i + 1));
                        numbers.set(i + 1, Double.toString(res));
                        numbers.remove(i);
                        operations.remove(i);
                        break;
                    }
                    if (operations.get(i).equals("-")) {
                        res = Double.parseDouble(numbers.get(i)) - Double.parseDouble(numbers.get(i + 1));
                        numbers.set(i + 1, Double.toString(res));
                        numbers.remove(i);
                        operations.remove(i);
                        break;
                    }
                }
            }
        }
        // in the end, the answer is the zero element of the list of numbers
        return numbers.get(0);
    }

}
