package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        // unusual input processing
        if (x == null || y == null)
            throw new IllegalArgumentException();
        else {
            int xSize = x.size();
            int ySize = y.size();
            if (xSize == 0)
                return true;
            else if (ySize == 0)
                return false;
            else {
                // main part
                int yStart = 0;
                boolean result = true;
                for (int i = 0; i < xSize; i++) {
                    if (result == true) {
                        for (int j = yStart; j < ySize; j++) {
                            // if an object from x is found in y, it always exits the loop by break
                            if (x.get(i) == y.get(j)) {
                                yStart = j + 1;
                                break;
                            }
                            // if the search in the y array is completed, but did not exit the loop, it will return false
                            else if (j == ySize - 1)
                                result = false;
                        }
                    }
                    else
                        break;
                }
                return result;
            }
        }
    }
}
