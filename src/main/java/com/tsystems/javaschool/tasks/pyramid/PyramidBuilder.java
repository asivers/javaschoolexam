package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int x = inputNumbers.size();
        // checks for null elements
        for (int i = 0; i < x; i++)
            if (inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        // checks if it is possible to build a pyramid with a given number of list items
        if (Math.pow(Math.floor(Math.sqrt(x * 2)), 2) + Math.floor(Math.sqrt(x * 2)) != x * 2)
            throw new CannotBuildPyramidException();
        // sorts list in ascending order
        Collections.sort(inputNumbers);
        // determining the number of rows and columns in the resulting array
        int rows = (int)Math.floor(Math.sqrt(x * 2));
        int columns = rows * 2 - 1;
        int[][] result = new int[rows][columns];
        int count = 0;
        // filling the pyramid
        for (int i = 0; i < rows; i++) {
            boolean odd = true;
            for (int j = (columns - 1) / 2 - i; j < (columns + 1) / 2 + i; j++) {
                if (odd) {
                    result[i][j] = inputNumbers.get(count);
                    count++;
                    odd = false;
                }
                else if (!odd) {
                    odd = true;
                }
            }
        }
        return result;
    }

}
